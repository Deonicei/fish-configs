# containers-storage-set-names
# Autogenerated from man page /usr/lib/jvm/default/man/man1/containers-storage-set-names.1.gz
complete -c containers-storage-set-names -s n -l name -d 'Specifies a name to set on the layer, image, or container'

