# fsck
# Autogenerated from man page /usr/lib/jvm/default/man/man8/fsck.minix.8.gz
complete -c fsck -l list
complete -c fsck -l repair
complete -c fsck -l auto
complete -c fsck -l super
complete -c fsck -s m -l uncleared
complete -c fsck -l force

