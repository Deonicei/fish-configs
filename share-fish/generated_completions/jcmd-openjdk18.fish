# jcmd-openjdk18
# Autogenerated from man page /usr/lib/jvm/default/man/man1/jcmd-openjdk18.1.gz
complete -c jcmd-openjdk18 -o all -d 'Compiler. codecache Prints code cache layout and bounds.  Impact: Low'
complete -c jcmd-openjdk18 -o parallel -d 'heap inspection'
complete -c jcmd-openjdk18 -o gz -d 'gzipped format using the given compression level'
complete -c jcmd-openjdk18 -o overwrite -d 'overwritten if it exists (BOOLEAN, false)'
complete -c jcmd-openjdk18 -s e -d 'false) [bu] 2'
complete -c jcmd-openjdk18 -s l -d '(BOOLEAN, false) VM'
complete -c jcmd-openjdk18 -s i -d '(BOOLEAN, false) [bu] 2'
complete -c jcmd-openjdk18 -s s -d subclasses
complete -c jcmd-openjdk18 -o verbose -d 'table (BOOLEAN, false) VM. symboltable [options] Dumps the symbol table'
complete -c jcmd-openjdk18 -o date -d '(BOOLEAN, false) VM. version Prints JVM version information.  Impact: Low'

