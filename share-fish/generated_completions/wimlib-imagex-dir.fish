# wimlib-imagex-dir
# Autogenerated from man page /usr/lib/jvm/default/man/man1/wimlib-imagex-dir.1.gz
complete -c wimlib-imagex-dir -l path -d 'List the files under PATH instead of under the root directory'
complete -c wimlib-imagex-dir -l detailed -d 'List detailed information about each file'
complete -c wimlib-imagex-dir -l one-file-only -d 'List information about the specified file only'
complete -c wimlib-imagex-dir -l ref -d 'File glob of additional WIMs or split WIM parts to reference resources from'

