# flatpak-builder
# Autogenerated from man page /usr/lib/jvm/default/man/man1/flatpak-builder.1.gz
complete -c flatpak-builder -s h -l help -d 'Show help options and exit'
complete -c flatpak-builder -s v -l verbose -d 'Print debug information during command processing'
complete -c flatpak-builder -l version -d 'Print version information and exit'
complete -c flatpak-builder -l arch -d 'Specify the machine architecture to build for'
complete -c flatpak-builder -l default-branch -d 'Set the default branch to BRANCH'
complete -c flatpak-builder -l disable-cache -d 'Don\\*(Aqt look at the existing cache for a previous build, instead always reb…'
complete -c flatpak-builder -l disable-rofiles-fuse -d 'Disable the use of rofiles-fuse to optimize the cache use via hardlink checko…'
complete -c flatpak-builder -l disable-download -d 'Don\\*(Aqt download any sources'
complete -c flatpak-builder -l disable-updates -d 'Download missing sources, but don\\*(Aqt update local mirrors of version contr…'
complete -c flatpak-builder -l disable-tests -d 'Don\\*(Aqt run any of the tests'
complete -c flatpak-builder -l run -d 'Run a command in a sandbox based on the build dir'
complete -c flatpak-builder -l build-shell -d 'Extract and prepare the sources for the named module, and then start a shell …'
complete -c flatpak-builder -l show-deps -d 'List all the (local) files that the manifest depends on'
complete -c flatpak-builder -l show-manifest -d 'Loads the manifest, including any included files and prints it in a canonical…'
complete -c flatpak-builder -l download-only -d 'Exit successfully after downloading the required sources'
complete -c flatpak-builder -l bundle-sources -d 'Create an additional runtime with the source code for this module'
complete -c flatpak-builder -l build-only -d 'Don\\*(Aqt do the cleanup and finish stages, which is useful if you want to bu…'
complete -c flatpak-builder -l finish-only -d 'Only do the cleanup, finish and export stages, picking up where a --build-onl…'
complete -c flatpak-builder -l export-only -d 'Only do the export stages, picking up the build result from a previous build'
complete -c flatpak-builder -l require-changes -d 'Do nothing, leaving a non-existent DIRECTORY if nothing changes since last ca…'
complete -c flatpak-builder -l state-dir -d 'Use this directory for storing state (downloads, build dirs, build cache, etc…'
complete -c flatpak-builder -l keep-build-dirs -d 'Don\\*(Aqt remove the sources and build after having built and installed each …'
complete -c flatpak-builder -l delete-build-dirs -d 'Always remove the sources and build after having built each module, even if t…'
complete -c flatpak-builder -l ccache -d 'Enable use of ccache in the build (needs ccache in the sdk)'
complete -c flatpak-builder -l stop-at -d 'Stop at the specified module, ignoring it and all the following ones in both …'
complete -c flatpak-builder -l repo -d 'After the build finishes, run flatpak build-export to export the result to th…'
complete -c flatpak-builder -s s -l subject -d 'One line subject for the commit message'
complete -c flatpak-builder -s b -l body -d 'Full description for the commit message'
complete -c flatpak-builder -l collection-id -d 'Set as the collection ID of the repository'
complete -c flatpak-builder -l token-type -d 'Set type of token needed to install this commit'
complete -c flatpak-builder -l gpg-sign -d 'Sign the commit with this GPG key.  Used when exporting the build results'
complete -c flatpak-builder -l gpg-homedir -d 'GPG Homedir to use when looking for keyrings'
complete -c flatpak-builder -l jobs -d 'Limit the number of parallel jobs during the build'
complete -c flatpak-builder -l force-clean -d 'Erase the previous contents of DIRECTORY if it is not empty'
complete -c flatpak-builder -l sandbox -d 'Disable the possibility to specify build-args that are passed to flatpak build'
complete -c flatpak-builder -l allow-missing-runtimes -d 'Do not immediately fail if the sdk or platform runtimes are not installed on …'
complete -c flatpak-builder -l rebuild-on-sdk-change -d 'Record the exact version of the sdk in the cache, and rebuild everything if i…'
complete -c flatpak-builder -l skip-if-unchanged -d 'If the json is unchanged since the last build of this filename, then do nothi…'
complete -c flatpak-builder -l mirror-screenshots-url -d 'Mirror any screenshots in the appstream and rewrite the appstream xml as if t…'
complete -c flatpak-builder -l extra-sources -d 'When downloading sources (archives, files, git, bzr, svn), look in this direc…'
complete -c flatpak-builder -l extra-sources-url -d 'When downloading sources (archives, files, git, bzr, svn), look at this url f…'
complete -c flatpak-builder -l from-git -d 'Look for the manifest in the given git repository'
complete -c flatpak-builder -l from-git-branch -d 'The branch to use with --from-git'
complete -c flatpak-builder -l no-shallow-clone -d 'Don\\*(Aqt use shallow clones when mirroring git repos'
complete -c flatpak-builder -l add-tag -d 'Add this tag to the tags list of the manifest before building'
complete -c flatpak-builder -l remove-tag -d 'Remove this tag to the tags list of the manifest before building'
complete -c flatpak-builder -l install-deps-from -d 'Install/update build required dependencies from the specified remote'
complete -c flatpak-builder -l install-deps-only -d 'Stop after downloading dependencies'
complete -c flatpak-builder -l install -d 'When the build is finished, install the result locally'
complete -c flatpak-builder -l user -d 'Install the dependencies in a per-user installation'
complete -c flatpak-builder -l system -d 'Install the dependencies in the default system-wide installation'
complete -c flatpak-builder -l installation -d 'Install the dependencies in a system-wide installation specified by NAME amon…'

