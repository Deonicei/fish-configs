# arlatex
# Autogenerated from man page /usr/lib/jvm/default/man/man1/arlatex.1.gz
complete -c arlatex -l version -d 'Output the arlatex script\'s version number'
complete -c arlatex -l help -d 'Output brief arlatex usage information'
complete -c arlatex -l document -d 'Specify the master document'

