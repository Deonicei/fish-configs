# pdfbook2
# Autogenerated from man page /usr/lib/jvm/default/man/man1/pdfbook2.1.gz
complete -c pdfbook2 -l version -d 'show program\'s version number and exit'
complete -c pdfbook2 -s h -l help -d 'show help message and exit  . SS GENERAL'
complete -c pdfbook2 -s p -l paper -d 'Format of the output paper dimensions as latex keyword (e. g'
complete -c pdfbook2 -s s -l short-edge -d 'Format the booklet for short-edge double-sided printing'
complete -c pdfbook2 -s n -l no-crop -d 'Prevent the cropping to the content area  . SS MARGINS'
complete -c pdfbook2 -s o -l outer-margin -d 'Defines the outer margin in the booklet (default: 40)'
complete -c pdfbook2 -s i -l inner-margin -d 'Defines the inner margin between the pages in the booklet (default: 150)'
complete -c pdfbook2 -s t -l top-margin -d 'Defines the top margin in the booklet (default: 30)'
complete -c pdfbook2 -s b -l bottom-margin -d 'Defines the bottom margin in the booklet (default: 30)  . SS ADVANCED'
complete -c pdfbook2 -l signature -d 'Define the signature for the booklet handed to pdfjam, needs to be multiple o…'
complete -c pdfbook2 -l 'signature*' -d 'Same as --signature'
complete -c pdfbook2 -l resolution -d 'Resolution used by ghostscript in bp (default: 72)'

