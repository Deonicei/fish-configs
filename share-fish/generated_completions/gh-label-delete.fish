# gh-label-delete
# Autogenerated from man page /usr/lib/jvm/default/man/man1/gh-label-delete.1.gz
complete -c gh-label-delete -l confirm -d 'Confirm deletion without prompting OPTIONS INHERITED FROM PARENT COMMANDS'
complete -c gh-label-delete -s R -l repo -d 'Select another repository using the [HOST/]OWNER/REPO format SEE ALSO'

