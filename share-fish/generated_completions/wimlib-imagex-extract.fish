# wimlib-imagex-extract
# Autogenerated from man page /usr/lib/jvm/default/man/man1/wimlib-imagex-extract.1.gz
complete -c wimlib-imagex-extract -l check -d 'Before extracting the files, verify the integrity of WIMFILE if it contains e…'
complete -c wimlib-imagex-extract -l ref -d 'File glob of additional WIMs or split WIM parts to reference resources from'
complete -c wimlib-imagex-extract -l dest-dir -d 'Extract the files and directories to the directory DIR instead of to the curr…'
complete -c wimlib-imagex-extract -l to-stdout -d 'Extract the files to standard output instead of to the filesystem'
complete -c wimlib-imagex-extract -l unix-data -d 'See the documentation for this option to wimapply(1)'
complete -c wimlib-imagex-extract -l no-acls -d 'See the documentation for this option to wimapply(1)'
complete -c wimlib-imagex-extract -l strict-acls -d 'See the documentation for this option to wimapply(1)'
complete -c wimlib-imagex-extract -l no-attributes -d 'See the documentation for this option to wimapply(1)'
complete -c wimlib-imagex-extract -l include-invalid-names -d 'See the documentation for this option to wimapply(1)'
complete -c wimlib-imagex-extract -l no-globs -d 'Do not recognize wildcard characters in paths'
complete -c wimlib-imagex-extract -l nullglob -d 'If a glob does not match any files, ignore it and print a warning instead of …'
complete -c wimlib-imagex-extract -l preserve-dir-structure -d 'When extracting paths, preserve the archive directory structure instead of ex…'
complete -c wimlib-imagex-extract -l wimboot -d 'See the documentation for this option to wimapply(1)'
complete -c wimlib-imagex-extract -l compact -d 'See the documentation for this option to wimapply(1)'
complete -c wimlib-imagex-extract -l no-wildcards
complete -c wimlib-imagex-extract -l recover-data -d 'See the documentation for this option to wimapply(1)'

