# gnuplot
# Autogenerated from man page /usr/lib/jvm/default/man/man1/gnuplot.1.gz
complete -c gnuplot -s p -l persist
complete -c gnuplot -s c
complete -c gnuplot -s d -l default
complete -c gnuplot -s e
complete -c gnuplot -s s -l slow
complete -c gnuplot -s h -l help
complete -c gnuplot -s V -d 'X11 OPTIONS For terminal type x11, gnuplot accepts the standard X Toolkit opt…'

