# wimdir
# Autogenerated from man page /usr/lib/jvm/default/man/man1/wimdir.1.gz
complete -c wimdir -l path -d 'List the files under PATH instead of under the root directory'
complete -c wimdir -l detailed -d 'List detailed information about each file'
complete -c wimdir -l one-file-only -d 'List information about the specified file only'
complete -c wimdir -l ref -d 'File glob of additional WIMs or split WIM parts to reference resources from'

