# setconf
# Autogenerated from man page /usr/lib/jvm/default/man/man1/setconf.1.gz
complete -c setconf -s v -d 'displays the current version number'
complete -c setconf -s h -l help -d 'displays brief usage information'
complete -c setconf -s t -l test -d 'performs internal self testing'
complete -c setconf -s a -l add -d 'adds an option, if not already present'
complete -c setconf -s d -l define -d 'changes a single-line #define value'
complete -c setconf -s u -l uncomment -d 'uncomments a key before changing the value'

