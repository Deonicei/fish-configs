if status is-interactive
    # Commands to run in interactive sessions can go here
end

# ROS1
# source /opt/ros/noetic/share/rosbash/rosfish
# bass source /opt/ros/noetic/setup.bash
# bass source ~/Ros/gleb/devel/setup.bash

# ROS 2
# export ROS_DOMAIN_ID=42
# bass source /usr/share/gazebo/setup.sh
# bass source /opt/ros2/foxy/setup.bash
if test -e /opt/ros2/galactic/setup.bash 
  bass source /opt/ros2/galactic/setup.bash 
  register-python-argcomplete --shell fish ros2 | source
end 

# fuck command helps you fix your mistakes

if test (which thefuck)
  thefuck --alias | source
else
  set_color grey
  printf 'install '
  set_color red 
  printf 'thefuck'
  set_color  grey  
  printf ' using \'pip install thefuck --user\'\n'
end  

# config for bobthefish theme
# omf theme bobthefish
set -g theme_color_scheme nord
set -g theme_display_vagrant yes
set -g theme_display_docker_machine yes
set -g theme_display_k8s_context yes
set -g theme_display_hg yes
set -g theme_display_virtualenv yes
set -g theme_display_nix no
set -g theme_display_ruby no
set -g theme_display_node yes
set -g fish_prompt_pwd_dir_length 0
set -g theme_display_hostname ssh
set -g theme_display_jobs_verbose yes
set -g theme_show_exit_status yes
set -g theme_display_user yes
set -g theme_display_hostname yes
set -g theme_newline_cursor yes
set -g theme_nerd_fonts yes
set -g theme_powerline_fonts no
set -g theme_color_scheme terminal-dark 

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
if test -e /opt/miniconda3/bin/conda
    /opt/miniconda3/bin/conda config --set auto_activate_base false
    /opt/miniconda3/bin/conda config --set changeps1 False
    source /opt/miniconda3/etc/fish/conf.d/conda.fish 
end
# <<< conda initialize <<<

export ROS_DOMAIN_ID=100
export ROVER_NAME=DEFAULT
export HOST_PLATFORM=linux_amd64_with_cuda

set -gx no_execute 1
set -g TERM 'screen-256color'
set -gx BAT_THEME Nord 
set -gx EDITOR nvim

if test -e ~/.config/fish/.secret_env
    source ~/.config/fish/.secret_env

    send_notify_to_server
    emit send_notify_to_server fish_postexec
else 
    set_color green
    echo "Setup ~/.config/fish/.secret_env like ~/.config/fish/secret_env_template!"
end


