# Fish Shell

## Installations

### Ubuntu 20

``` 
  sudo apt-get install ./fish-configs-ristle.deb -y
``` 

### ArchLinux
``` 
  sudo pacman -U ./fish-configs-ristle-0.0.2-1-any.pkg.tar --noconfirm 
```

### Usage 

[Fish Shell](https://fishshell.com) is a modern shell interpretator with whole bunch of usefull features. So, here u can find my own configs

![shell](./.gitlab/fishshell.png)

