function setDisbot
	if [ -z $1 ]
		cd ~/Ros/gleb
	else 
		cd $1
	end 

    set -l inet (ifconfig wlo1 |grep 'inet '|awk '{print $2}')
    if [ -z $inet ]
        set inet (ifconfig enp0s20f0u4 |grep 'inet '|awk '{print $2}')
    end

    if [ -z $inet ]
        set_color red
        printf "Not found ip!"
        set_color white
        set inet 127.0.0.1
        set master "http://127.0.0.1:11311"
    end
	export ROS_IP=$inet

	if [ -z $master ]
        set master "http://10.0.0.12:11311"
	end
	export ROS_MASTER_URI=$master
end
