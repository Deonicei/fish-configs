function repeat_cmd_until_done
  set -l  b 1; 
    while test $b = 1
        fish -c "$argv" 
            if test $status -eq 0 
                  set -l b 2 && return 
                      end
                          sleep 0.5
                            end
                            end

