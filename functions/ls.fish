function ls --wraps='exa --long --header --git --icons' --wraps='exa --long --header --git ' --description 'alias ls=exa --long --header --git '
    if test (which exa)
        exa --long --header --git --icons $argv
    else
        /usr/bin/ls --color  $argv
    end
end
