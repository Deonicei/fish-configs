function kill_all

    if test (ps -A |grep -E $argv[1]|wc -l) -eq 0
        set_color red
        echo "No active processes with names \"$argv\" now"
        set_color normal
        return 1
    end
    set_color green
    echo "Do u want to kill this processes:"
    echo ""

    set_color normal
    ps -A | grep $argv[1]

    read -l -P 'Do you want to continue? [y/N] ' confirm
    switch $confirm
        case Y y
            for name in (ps -A |grep -E $argv[1] |awk '{print $1}')
                kill -9 $name
                if test $status -ge 1
                    sudo kill -9 $name
                end
            end
        case '' N n
            set_color red
            echo "Exiting..."
            set_color normal
            return 0
    end
end
