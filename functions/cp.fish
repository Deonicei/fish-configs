function cp --wraps='advcp -gR' --description 'alias cp=advcp -gR'
  if test -e /usr/bin/advcp 
    advcp -gR $argv; 
  else 
    /usr/bin/cp -r $argv;
  end 
end
