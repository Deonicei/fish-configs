function gm --description 'add [f-task_number]'
    set branch_name_not_formated (string sub -e -1 (string sub -s 3  (git branch |grep \* |awk '{print $2}' ) |tr '-' '\n' | head -n2)
set branch_name (string sub -e -1 (string sub -s 3  (git branch |grep \* |awk '{print $2}' ) |tr '-' '\n' | head -n2 |tr '\n'  '-')))

    if not test -z branch_name_not_formated
        #echo "[$branch_name]"
        #echo "giit commit -m \"[$branch_name] $argv\" [y/N]"
        if not test -z (string match -r "CSD" $branch_name)
            git commit -m "[$branch_name] $argv"
        else
            git commit -m "$argv"
        end
    else
        echo "no task"
    end
end
