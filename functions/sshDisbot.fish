function sshDisbot
    set wifi_name (nmcli -t -f active,ssid dev wifi | egrep '^yes' | cut -d\' -f2 | awk -F '[:]' '{print $2}')
    set ip (echo $wifi_name |awk -F '[t_]' '{print $2}')
    set password "disbot2021+V$ip"
    set_color green
    printf "Trying to connect to Disbot $ip"
    set_color white
    if test -z $argv[1]
        set ip_address 10.0.0.12
    else
        set ip_address 10.0.0.11
    end

    sshpass -p $password ssh ubuntu@$ip_address
end
