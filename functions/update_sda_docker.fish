function update_sda_docker --description 'Manage my sda container'
    #!/usr/bin/fish

    function error_handler
        status --print-stack-trace
        return 1 # means not run anything more in the block, return 0 for ignoring error
    end

    begin
        set -l options n/no_stop 's/sda=' 'f/fish='

        argparse $options -- $argv

        set --query _flag_sda; or set -l _flag_sda /home/$USER/Programming/AutoTech/sda
        set --query _flag_fish; or set -l _flag_fish /home/$USER/Programming/AutoTech/fish-deps

        if test -z (string match $PWD $_flag_sda)
            cd $_flag_sda
        end

        if test (docker ps |grep sda-ubuntu |grep Up |wc -l) -ge 1
            and not set --query _flag_no_stop
            set_color blue
            echo "Stopping container"
            set_color green
            ./c s

            sleep 1.0
            set_color normal
            ./c build-image --with-simulator True

            set_color blue
            echo "Starting container..."
            set_color normal

            sleep 1.0
        else
            ./c build-image --with-simulator True
        end

        ./c start --no-artifacts-fetch

        set docker_name (docker ps |grep sda-ubuntu |grep Up | tr ' ' '\n' |tail -n1)
        if not test -z $docker_name

            set -l docker_name (docker ps |grep sda-ubuntu |grep Up | tr ' ' '\n' |tail -n1)
            set installed_ (docker exec  -it $docker_name bash -c "sudo apt show my-fish-configs 2>/dev/null" |wc -l )

            if test $installed_ -eq 0
                docker exec -it $docker_name bash -c "[ -d \"/clion-2021.2.1/\" ] && sudo rm -rf /clion-2021.2.1 && sudo mv -if /rep/clion* /"
                
                ping -c1 artifactory.sberautotech.ru > /dev/null
                set ping_server (echo $status)

                if test $ping_server -eq 0
                  docker exec -it $docker_name bash -c "sudo apt-get update -y && sudo apt-get install sudo vim gnupg wget software-properties-common -y && 
                    wget -O - https://ristle.ru/ubuntu/keyFile | sudo apt-key add - && 
                    sudo add-apt-repository 'deb https://ristle.ru/ubuntu/ stable main' && 
                    sudo apt-get update -y  && 
                    sudo apt-get install fish-config-ristle -y && 
                    su - $USER -c install_ristle_fish_shell; 
                    sudo apt-get update -y && sudo apt-get install -y bat=0.1 exa screen;
                    sed -i '900s//    set -l files $argv[2..-1]/g' /home/$USER/.local/share/omf/themes/bobthefish/functions/fish_prompt.fish"
                else
                  docker exec -it $docker_name bash -c "sudo apt-get install /rep/log/apt-deps/*deb -y"
                  docker exec -it $docker_name bash -c "cp -r /rep/log/.local/ ~/ && cp -r /rep/log/.config/ ~/"
                end
                docker exec -it $docker_name bash -c "rm /home/$USER/.local/share/fish/fish_history; sudo ln -sr /rep/log/fish_history /home/$USER/.local/share/fish/fish_history"
                docker exec -it $docker_name bash -c "mkdir -p /rep/log/.config/JetBrains; mkdir -p /rep/log/.local/share/JetBrains; touch /rep/log/fish_history"
                docker exec -it $docker_name bash -c "sudo ln -sr /rep/log/.config/JetBrains /home/$USER/.config && 
                                                      sudo ln -sr /rep/log/.local/share//JetBrains  /home/$USER/.local/share/ &&
                                                      sudo chown $USER ~/. -R"

                # git clone https://github.com/artofnothingness/configs.git log/configs 
#                if not test -e log/configs
#                    git clone https://gitlab.com/ristle/configs.git log/configs
#                end
#                docker exec -it $docker_name bash -c "cd /rep/log/configs/ && ./configure.sh  --nvim" 
            else
                set_color blue
                echo "Fish configs already installed!"
                set_color normal
            end
        else
            set_color red
            echo "Contaiter not started properly!"
            set_color normal
        end
    end
end
