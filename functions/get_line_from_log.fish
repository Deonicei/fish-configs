function get_line_from_log
    set -l options h/help 'f/file=' 't/time='

    argparse $options -- $argv
    if set --query _flag_help
        set_color blue
        echo "- f/file = file location to ros log"
        echo "- t/time = set time in seconds from fimr in ros log"
        return 0
    end
    if set --query _flag_help
        set_color blue
        echo "- f/file = file location to ros log"
        echo "- t/time = set time in seconds from fimr in ros log"
        return 0
    end

    if not set --query _flag_file && not set --query _flag_time
        set_color red
        echo "Specify file and time to cat!"
        set_color normal
        return 1
    end

    function get_time_from_line
        set -l unix_time (string sub -s 2 -e -1 (echo $argv[1] |awk '{print $2}' ))

        set -l generic_time (date -d @$unix_time)
        set -l time (echo $generic_time | awk '{print $4}')
        set -l minutes (echo $time | awk -F '[:]' '{print $2}')
        set -l seconds (echo $time | awk -F '[:]' '{print $3}')

        set -l overall_time (math \( $minutes \* 60 + $seconds\))
        echo $overall_time
    end

    set -l start_time (get_time_from_line (head -n 1 $_flag_file))

    for name in (cat $_flag_file)
        if test -z $name
            continue
        end

        set -l overall_time (get_time_from_line $name)

        if test (math $overall_time - $start_time) -ge $_flag_time
            set_color green
            echo $name
            set_color normal
            break
        end
    end
end
