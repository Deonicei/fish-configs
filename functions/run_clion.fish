function run_clion
    if [ -z $argv[1] ]
        set programm "clion"
    else
        set programm "pycharm-professional"
    end
    if [ -z $argv[3] ]
        set folder .
    else
        set folder $argv[3]
    end
    if [ -z $argv[2] ]
        set name "clion"
    else
        set name $argv[2]
    end

    screen -d -m -S $name $programm $folder
end
