function send_notify_to_server --on-event fish_postexec
    set -gx list_of_access docker podman build.py python pacman

    set -l idetime (xprintidle)
    set -l threshold 60000 # 1 min

    for value in $argv
        if contains $value $list_of_access && test $idetime -gt $threshold
            push_gotify -t (string upper (string sub -e 1 $value))(string sub -s 2 $value) -m $argv
        end
    end
end
