function gl --wraps='git clone' --description 'alias gl=git clone'
  git clone $argv; 
end
