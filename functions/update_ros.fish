function update_ros --description 'Rebuilding nessesary packages after upgrading'
    set_color green
    printf "Updating whole system\n"
    set_color normal
    paru -Syu --noconfirm --sudoloop

    set_color green
    printf "Starting updating broken packages\n"
    set_color normal

    python ~/ros-find-outofdate.py >/tmp/need_to_rebuild
    rm /tmp/broken_packages
    touch /tmp/broken_packages
    for name in (cat /tmp/need_to_rebuild)
        paru -Syu $name --sudoloop --aur --rebuild --noconfirm
        if test $status
            echo $name >>/tmp/broken_packages

            set_color red
            printf "\n Broken package: "$name"\n"
            set_color normal
        end
    end
end
