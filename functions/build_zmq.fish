function build_zmq --description 'Building all zmq nodes, which are based on ROS'
set -l options 'f/folder_build='
argparse $options -- $argv
set --query _flag_folder_build; or set -l _flag_folder_build build
set all_packages (find src/ \( -name "package.xml" -not  -name "*build*" \))
for name in $all_packages
set package_name (string split -r -m2 / $name | head -n2 | tail -n1)
set_color green
echo "[Processing]: "$package_name
set_color normal
build_this_project -s (string split -r -m1 / $name |head -n1) -f build/$package_name
end
end
