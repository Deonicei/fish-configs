function __bobthefish_cmd_duration --description 'Show command duration' --no-scope-shadowing
                               [ "$theme_display_cmd_duration" = no ]
                               and return

                               [ -z "$CMD_DURATION" -o "$CMD_DURATION" -lt 100 ]
                               and return

                               if [ "$CMD_DURATION" -lt 5000 ]
                                   echo -ns $CMD_DURATION ms
                               else if [ "$CMD_DURATION" -lt 60000 ]
                                   __bobthefish_pretty_ms $CMD_DURATION s
                               else if [ "$CMD_DURATION" -lt 3600000 ]
                                   set_color $fish_color_error
                                   __bobthefish_pretty_ms $CMD_DURATION m
                               else
                                   set_color $fish_color_error
                                   __bobthefish_pretty_ms $CMD_DURATION h
                               end

                               set_color $fish_color_normal
                               set_color $fish_color_autosuggestion

                               [ "$theme_display_date" = no ]
                               or echo -ns ' ' $__bobthefish_left_arrow_glyph
                           
end
