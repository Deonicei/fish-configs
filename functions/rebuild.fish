function rebuild --wraps='paru -S --rebuild --aur --noconfirm --sudoloop' --description 'alias rebuild=paru -S --rebuild --aur --noconfirm --sudoloop'
  paru -S --rebuild --aur --noconfirm --sudoloop $argv; 
end
