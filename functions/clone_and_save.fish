function clone_and_save
set --query n; or set -l n $argv[2]
wget $argv[1] -O $n.mp4
set -g n (math $n + 1)
end
