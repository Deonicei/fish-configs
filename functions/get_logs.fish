# Defined in - @ line 1
function get_logs --wraps=ll\ -al\ --sort=time\ \~/start/logs/\ \|grep\ nuc\ \|head\ -n2\ \|tail\ -n1\ \|\ awk\ \'\{print\ \}\'\  --description alias\ get_logs=ll\ -al\ --sort=time\ \~/start/logs/\ \|grep\ nuc\ \|head\ -n2\ \|tail\ -n1\ \|\ awk\ \'\{print\ \}\'\ 
  ll -al --sort=time ~/start/logs/ |grep nuc |head -n2 |tail -n1 | awk '{print }'  $argv;
end
