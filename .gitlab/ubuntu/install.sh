#!/usr/bin/env bash
echo $PWD 
echo $pwd 

echo `pwd`
export srcdir=$PWD 
export pkgdir=$PWD  

rm -rf $srcdir/completions
rm -rf $srcdir/conf.d
rm -rf $srcdir/config.fish
rm -rf $srcdir/fish_plugins
rm -rf $srcdir/fish_variables
rm -rf $srcdir/functions
rm -rf $srcdir/omf
rm -rf $srcdir/omf-config
rm -rf $srcdir/share-fish

cp -r $srcdir/../../completions $srcdir
cp -r $srcdir/../../conf.d $srcdir
cp -r $srcdir/../../config.fish $srcdir
cp -r $srcdir/../../fish_plugins $srcdir
cp -r $srcdir/../../fish_variables $srcdir
cp -r $srcdir/../../functions $srcdir
cp -r $srcdir/../../omf $srcdir
cp -r $srcdir/../../omf-config $srcdir
cp -r $srcdir/../../local-omf $srcdir
cp -r $srcdir/../../share-fish $srcdir

mkdir -p $pkgdir//usr/local/share/ristle-fish-configs 

cp -r $srcdir/* $pkgdir/usr/local/share/ristle-fish-configs

mkdir -p $pkgdir/usr/local/bin 

cp -r $srcdir/../install_ristle_fish_shell $pkgdir/usr/local/bin 
chmod +X $pkgdir/usr/local/bin 

echo
echo "Thanks for using Ristle's configs!"
echo "Run 'install_ristle_fish_shell' to set up fish configs!"
echo 
